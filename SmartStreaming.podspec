Pod::Spec.new do |s|
  s.name             = 'SmartStreaming'
  s.version          = '1.0.0'
  s.summary          = 'Trying to solve cocoapod issue'
  s.description      = 'Facing lot of issue while creating cocoapods, trying to solve'

  s.homepage         = 'https://bitbucket.org/vetri-mediamelon/smartstreaming-cocoapods'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'vetri' => 'vetri@mediamelon.com' }
  s.source           = { :git => 'https://bitbucket.org/vetri-mediamelon/smartstreaming-cocoapods', :tag => s.version.to_s }
  s.swift_version    = '4.0'
  s.ios.deployment_target = '10.0'
  s.source_files = 'SmartStreaming/Classes/**/*.{h,swift}'
  s.frameworks = 'UIKit', 'AVFoundation', 'CoreTelephony'
  s.vendored_libraries = 'SmartStreaming/Classes/SmartStreaming/libmmsmartstreamer.a'
  s.libraries = 'stdc++'
  s.public_header_files = 'SmartStreaming/Classes/SmartStreaming/**/*.h'
end
