# SmartStreaming

[![CI Status](https://img.shields.io/travis/vetri/SmartStreaming.svg?style=flat)](https://travis-ci.org/vetri/SmartStreaming)
[![Version](https://img.shields.io/cocoapods/v/SmartStreaming.svg?style=flat)](https://cocoapods.org/pods/SmartStreaming)
[![License](https://img.shields.io/cocoapods/l/SmartStreaming.svg?style=flat)](https://cocoapods.org/pods/SmartStreaming)
[![Platform](https://img.shields.io/cocoapods/p/SmartStreaming.svg?style=flat)](https://cocoapods.org/pods/SmartStreaming)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

SmartStreaming is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'SmartStreaming'
```

## Author

vetri, vetri@mediamelon.com

## License

SmartStreaming is available under the MIT license. See the LICENSE file for more info.
